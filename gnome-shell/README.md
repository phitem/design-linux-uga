# Theme UGA pour Gnome-shell

## Ce que ça fait

 * Change le theme Ubuntu (Yaru) aux couleurs de l'UGA

## Utilisation



## Licences

 * Le code source est sous licence Apache 2.0.


## Modification du thème GDM par patch sur le paquet yaru-theme-gnome-shell

```bash
echo "deb-src http://archive.ubuntu.com/ubuntu focal main restricted
deb-src http://archive.ubuntu.com/ubuntu focal-updates main restricted
" | sudo tee /etc/apt/sources.list.d/ubuntu-src.list

sudo apt update

sudo apt install devscripts fakeroot

sudo apt-get build-dep yaru-theme-gnome-shell

apt-get source yaru-theme-gnome-shell # sans sudo !

cd yaru-theme*

[faire modif / applique patch]

dch -i # noter les modifications

dpkg-buildpackage -rfakeroot

sudo apt install ../<paquet>.deb

# sudo systemctl restart gdm3

```
