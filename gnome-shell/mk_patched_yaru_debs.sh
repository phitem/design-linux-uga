#!/usr/bin/env bash
set -euo pipefail

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Dépendances build
apt install -y debhelper dh-migrations libgtk-3-dev git meson sassc

# Dernière version Yaru 20.04
git clone -b '20.04.11' --single-branch --depth 1 https://github.com/ubuntu/yaru.git

# Applique les modifications

## Palette générale
find yaru/ -name "*ubuntu-colors.scss" -exec cp "$SCRIPT_DIR/palette.scss" {} \;
## Fond terminal
sed -i 's/$_terminal_bg_color: #300A24/$_terminal_bg_color: #10152b/' yaru/gtk/src/default/gtk-3.20/_apps.scss


#Building binary debian files
cd yaru
dpkg-buildpackage --build=binary -uc -us -tc