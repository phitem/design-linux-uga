# Fonds d'écran et éléments de design UGA pour Linux

**Statut** : stable

Ce dépôt contient des fonds d'écran et des éléments de design Université Grenoble Alpes
pour Linux.

Compatibilité :
 * Ubuntu 20.04

## Contenu

 * Fonds d'écran et un diaporama pour Gnome
 * Logo UGA pour plymouth
 * Logo UGA pour GDM3
 * Icônes

## Utilisation

Des paquets **.deb** sont disponibles : voir les [releases](https://gricad-gitlab.univ-grenoble-alpes.fr/phitem/design-linux-uga/-/releases).

Si vous utilisez les paquets de thème Yaru, vous devrez les figer pour éviter que les mises à jour Ubuntu les écrasent :

```bash
sudo apt-mark hold yaru*
```

Pour chaque élément de design, voir le README du dossier correspondant.

## Licences

 * Le code source est sous licence Apache 2.0.
 * Pour le thème Yaru utilisé comme base, voir https://github.com/ubuntu/yaru
 * Le logo UGA est la propriété de l'Université Grenoble Alpes et ne peut être modifié.

Les photographies sont tirées de la banque d'image unsplash.com :
  * https://unsplash.com/photos/aL7SA1ASVdQ
  * https://unsplash.com/photos/6egdNN_3k3I
  * https://unsplash.com/photos/GRXAoEXsvsY
  * https://unsplash.com/photos/Hld-BtdRdPU
  * https://unsplash.com/photos/2Bjq3A7rGn4
  * https://unsplash.com/photos/-hI5dX2ObAs
  * https://unsplash.com/photos/h7PJC8pvmHI
