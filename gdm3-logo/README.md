# Logo UGA pour GDM3

## Ce que ça fait

 * Remplace le logo Ubuntu sur l'écran de login

## Utilisation

```bash
install -m 644 logo.png /usr/share/pixmaps
install -m 644 dconf_profile  /etc/dconf/profile/gdm
mkdir /etc/dconf/db/gdm.d
install -m 644 dconf_02-logo /etc/dconf/db/gdm.d/02-logo
dconf update
```

## Licences

 * Le code source est sous licence Apache 2.0.
 * Le logo UGA est la propriété de l'Université Grenoble Alpes et ne peut être modifié.


## Modification du thème GDM par modification du fichier gnome-shell-theme.gresource (non implémenté)

Il n'est pas très simple de modifier le thème de GDM. Le thème est définit dans un binaire (par défaut `/usr/share/gnome-shell/gnome-shell-theme.gresource`).

[Cette méthode](https://wiki.archlinux.org/title/GDM#Login_screen_background_image) permet de faire des changements.

### Exemple sur Ubuntu 20.04

* Extraire le contenu du binaire gresource

```bash
#!/bin/sh
gst=/usr/share/gnome-shell/gnome-shell-theme.gresource
workdir=${HOME}/shell-theme
for r in `gresource list $gst`; do r=${r#\/org\/gnome\/shell/}; if [ ! -d $workdir/${r%/*} ]; then   mkdir -p $workdir/${r%/*}; fi; done
for r in `gresource list $gst`; do         gresource extract $gst $r >$workdir/${r#\/org\/gnome\/shell/}; done
```

* Modifier ensuite les fichiers souhaités dans `$HOME/shell-theme` (pour GDM sur Ubuntu 20.04 : gdm3.css)

Pour recréer le fichier gresource :

* Télécharger gnome-shell-theme.gresource.xml correspondant à la version de Gnome

```bash
wget https://gitlab.gnome.org/GNOME/gnome-shell/-/raw/3.36.4/data/gnome-shell-theme.gresource.xml
```

* Ajouter `<file>gdm3.css</file>` dans le gresource.xml (correspond à un [patch Ubuntu](https://git.launchpad.net/ubuntu/+source/gnome-shell/tree/debian/patches/ubuntu/gdm_alternatives.patch))

* Recopier les icônes (sinon message d'erreur)

```bash
cp icons/scalable/*/*.svg .
```

* Recompiler le binaire gresource

```bash
glib-compile-resources gnome-shell-theme.gresource.xml
```

* Remplacer le gresource original et relancer GDM

```bash
sudo cp gnome-shell-theme.gresource /usr/share/gnome-shell/gnome-shell-theme.gresource 
sudo systemctl restart gdm3
```

