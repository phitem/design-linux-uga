#!/usr/bin/env bash

set -e

FAKEROOT="$(pwd)/design-linux-uga"
rm -rf "$FAKEROOT"

mkdir -p "$FAKEROOT/usr/share/backgrounds/uga"
mkdir -p "$FAKEROOT/usr/share/initramfs-tools/conf-hooks.d"
mkdir -p "$FAKEROOT/usr/share/gnome-background-properties/"
mkdir -p "$FAKEROOT/usr/share/plymouth"
mkdir -p "$FAKEROOT/usr/share/pixmaps"
mkdir -p "$FAKEROOT/usr/share/icons"
mkdir -p "$FAKEROOT/usr/share/plymouth/themes/spinner"

mkdir -p "$FAKEROOT/DEBIAN"
cp -r debian/* "$FAKEROOT/DEBIAN"
sed -i "s/<version>/0.0/g" "$FAKEROOT/DEBIAN/control" # mets la version à 0.0 si il n'y en a pas
chmod 0755 "$FAKEROOT/DEBIAN/"*inst
chmod 0755 "$FAKEROOT/DEBIAN/"*rm

# fonds d'écrans

if ! which rsvg-convert; then
  echo "rsvg missing" && exit 1
fi
if ! which mogrify; then
  echo "imagemagick missing" && exit 1
fi

cd wallpapers
make
make INSTALL_ROOT="$FAKEROOT" install
cd -

# plymouth
install -m 644 plymouth/logo.png "$FAKEROOT/usr/share/plymouth/ubuntu-logo.png"
install -m 644 plymouth/logo.png "$FAKEROOT/usr/share/plymouth/themes/spinner/watermark.png"

# icons
install -m 644 icons/folder-uga.svg "$FAKEROOT/usr/share/icons"

# GDM3 Logo
install -m 644 gdm3-logo/logo.png "$FAKEROOT/usr/share/pixmaps"
mkdir -p "$FAKEROOT/etc/dconf/profile"
install -m 644 gdm3-logo/dconf_profile  "$FAKEROOT/etc/dconf/profile/gdm"
mkdir -p "$FAKEROOT/etc/dconf/db/gdm.d"
install -m 644 gdm3-logo/dconf_02-logo "$FAKEROOT/etc/dconf/db/gdm.d/02-logo"


dpkg-deb --build design-linux-uga
