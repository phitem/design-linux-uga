# Logo UGA pour Plymouth

## Ce que ça fait

 * Remplace le logo Ubuntu lors du démarrage (nécessite plymouth et un thème compatible installé).

## Comment ça marche

Les fichiers suivants sont remplacés :
  * `/usr/share/plymouth/ubuntu-logo.png`
  * `/usr/share/plymouth/themes/spinner/watermark.png`

On utilise `dpkg-divert` afin que ces fichiers ne soient pas réécrasés lors de la réinstallation / mise à jour des paquets dont ils proviennent. Voir https://www.debian.org/doc/debian-policy/ap-pkg-diversions.html.

## Utilisation

Installation :
```bash
# en root
dpkg-divert --add --rename --divert /usr/share/plymouth/themes/spinner/watermark.png.ubuntu /usr/share/plymouth/themes/spinner/watermark.png
dpkg-divert --add --rename --divert /usr/share/plymouth/ubuntu-logo.png.ubuntu /usr/share/plymouth/ubuntu-logo.png
install -m 644 logo.png /usr/share/plymouth/ubuntu-logo.png
install -m 644 logo.png /usr/share/plymouth/themes/spinner/watermark.png
update-initramfs -u
```

Désinstallation:
```bash
# en root
rm /usr/share/plymouth/ubuntu-logo.png
rm /usr/share/plymouth/themes/spinner/watermark.png
dpkg-divert --remove --rename --divert /usr/share/plymouth/themes/spinner/watermark.png.ubuntu /usr/share/plymouth/themes/spinner/watermark.png 	
dpkg-divert --remove --rename --divert /usr/share/plymouth/ubuntu-logo.png.ubuntu /usr/share/plymouth/ubuntu-logo.png
update-initramfs -u
```


## Problèmes connus

### Le logo ne s'affiche pas au démarrage

Vérifier que plymouth est installé avec un thème compatible. Le thème plymouth spinner est compatible (`apt install plymouth-theme-spinner`).

Vérifier que grub a bien `quiet splash` dans `/etc/default/grub`.

Si le logo du fabricant de l'ordinateur s'affiche, c'est que le thème bgrt est utilisé => `sudo update-alternatives --config default.plymouth`.

## Licences

 * Le code source est sous licence Apache 2.0.
 * Le logo UGA est la propriété de l'Université Grenoble Alpes et ne peut être modifié.
