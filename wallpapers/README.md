# Fonds d'écran UGA pour Linux


## Utilisation

Pour obtenir les fonds d'écran au format JPEG :

```bash
make
```

**Attention** : `make` nécessite librsvg2-bin et ImageMagick

Pour installer les fichers sur un système Linux :

```bash
make
sudo make install
```

Pour installer les fichers sur un système Linux :

```bash
make
sudo make install # sudo make uninstall pour désinstaller
```

Voir le fichier `Makefile` pour installer manuellement.

## Licences

 * Le logo UGA est la propriété de l'Université Grenoble Alpes et ne peut être modifié.

Les photographies sont tirées de la banque d'image unsplash.com :
  * https://unsplash.com/photos/aL7SA1ASVdQ
  * https://unsplash.com/photos/6egdNN_3k3I
  * https://unsplash.com/photos/GRXAoEXsvsY
  * https://unsplash.com/photos/Hld-BtdRdPU
  * https://unsplash.com/photos/2Bjq3A7rGn4
  * https://unsplash.com/photos/-hI5dX2ObAs
  * https://unsplash.com/photos/h7PJC8pvmHI
